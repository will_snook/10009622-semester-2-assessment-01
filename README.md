<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Welcome To Cars, Borders And Quotes! - 10009622</title>
        <link rel="stylesheet" href="css/styles.css" type="text/css" >
    </head>
    <body>
        
        <h1>Welcome To Cars, Borders And Quotes!<h1> 
        <dlv> 
        <img class="picture" src="image\cars01.jpg" alt="cars1" >
        <blockquote><h2> True faith discovered was When painted panel, statuary, Glass-mosaic, window-glass, Amended what was told awry By some peasant gospeler.</blockquote>
        <p>—  William Butler Yeats</p> 
     </dlv>
      <dlv>    

        <img class="picture" src="image\cars02.jpg" alt="cars2" >
        <blockquote><h2>As we read the school reports on our children, we realize a sense of relief, that can rise to delight, that, thank Heaven, nobody is reporting in this fashion on us.</blockquote>
        <p>—  John Boynton Priestley</p>
     </dlv>
      <dlv> 

        <img class="picture" src="image\cars03.jpg" alt="cars3" >
        <blockquote><h2>Television has proved that people will look at anything rather than each other.</blockquote>
        <p>—  Ann Landers</p>
     </dlv>
      <dlv>

        <img class="picture" src="image\cars04.jpg" alt="cars4" >
        <blockquote><h2>Get money, still get money, boy, no matter by what means.</blockquote>
        <p>—  Ben Jonson</p>
     </dlv>
      <dlv> 

        <img class="picture" src="image\cars05.jpeg"alt="cars5" >
        <blockquote><h2>No man but a blockhead ever wrote except for money.</blockquote>
        <p>—  Samuel Johnson</p> 
     </dlv>
      <dlv>

        <img class="picture" src="image\cars06.jpg" alt="cars6" >
        <blockquote><h2>Every great and commanding movement in the annals of the world is the triumph of enthusiasm. Nothing great was ever achieved without it.</blockquote>
        <p>—  Ralph Waldo Emerson</p>
     </dlv>
      <dlv>

        <img class="picture" src="image\cars07.jpg" alt="cars7" >
        <blockquote><h2>People can be more forgiving than you can imagine. But you have to forgive yourself. Let go of what's bitter and move on.</blockquote>
        <p>—  Bill William Henry Cosby</p>
     </dlv>
      <dlv>

        <img class="picture" src="image\cars08.jpg" alt="cars8" >
        <blockquote><h2>Honor's a thing too subtle for wisdom; if honor lie in eating, he's right honorable.</blockquote>
        <p>—  Francis Beaumont</p>
     </dlv>
      <dlv>

        <img class="picture" src="image\cars09.jpg" alt="cars9" >
        <blockquote><h2>BEHAVIOR, n. Conduct, as determined, not by principle, but by breeding.</blockquote>
        <p>—  Ambrose Gwinett Bierce</p> 
     </dlv>
      <dlv>

        <img class="picture" src="image\cars10.jpg" alt="cars10" > 
        <blockquote><h2>I tell you that as long as I can conceive something better than myself I cannot be easy unless I am striving to bring it in to existence or clearing the way for it.</blockquote>
        <p>—  George Bernard Shaw</p> 
     </dlv>
      
      
        
        
        <!-- Content ends here -->
        <script src=""></script>
    
    </body>
</html>
